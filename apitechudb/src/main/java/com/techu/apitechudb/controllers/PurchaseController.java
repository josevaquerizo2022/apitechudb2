package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
//Este req mapping puesto aquí es como una ruta general para todas las conexiones
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    /*    @GetMapping("/Purchases")
        public List<PurchaseModel> getPurchases(){
            System.out.println("getPurchases");

            return this.PurchaseService.findAll();
        }
     */

    @GetMapping("/purchase")
    public ResponseEntity<List<PurchaseModel>> getPurchase(){
        System.out.println("getPurchases");

        return new ResponseEntity<>(
                this.purchaseService.findAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/purchases/{id}")
    public ResponseEntity<Object> getPurchaseById(@PathVariable String id) {
        System.out.println("getPurchaseById");
        System.out.println("La id del Purchaseo a buscar es = " + id);

        Optional<PurchaseModel> result = this.purchaseService.findById(id);

        return  new ResponseEntity<>(
                result.isPresent() ? result.get(): "Purchase no econtrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );

        //(Condición) = vale_essto_si_true : vale_esto_si_false
    }

    @PostMapping("/purchase")
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase");
        System.out.println("La id de la compra a crear es " + purchase.getId());
        System.out.println("El comprador es " + purchase.getUserId());
        System.out.println("El total de la compra es " + purchase.getAmount());
        System.out.println("La id de la compra a crear es " + purchase.getPurchaseItems());

        return new ResponseEntity<>(
                this.purchaseService.add(purchase),
                HttpStatus.CREATED
        );
    }

}
