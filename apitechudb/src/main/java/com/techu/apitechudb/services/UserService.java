package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;


import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
//LISTAR TODO

    public List<UserModel> getUsers(String orderBy){
        System.out.println("getUsers en UserService");

        List<UserModel> result;

        if (orderBy != null){
            System.out.println("Se ha pedido ordenación");
            result = this.userRepository.findAll(Sort.by("age"));
        } else {
            result = this.userRepository.findAll();
        }

        return result;
    }

//FIN LISTAR TODO

    //ORDENAR
/*
    public List<UserModel> findAll(){
        System.out.println("findAll en UserService");

        return this.userRepository.findAll(sortByAgeAsc());

        private Sort sortByAgeAsc(){
            return new Sort(Sort.Direction.ASC, "age");
        }
    }

 */
    //FIN ORDENAR



    public Optional<UserModel> findById(String id){
        System.out.println("Optional en UserService");
/*        if (this.objectProperty != null)
            if (this.objectProperty.getProperty() != null)
                if (this.objectProperty.getProperty().getAnother() != null)
                    if (this.objectProperty.getProperty().getAnother().getYetAnotherProperty())*/

        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user){
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel userModel) {
        System.out.println("update en UserModel");

        return this.userRepository.save(userModel);
    }

    public boolean delete(String id) {

        System.out.println("delete en PS");
        boolean result = false;
        if (this.findById(id).isPresent() == true){
            System.out.println("Usuario a borrar encontrado");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
        //findById al ser un optional hay que validarlo
        //this.productRepository.findById(id); no se usa porque NO lleva implicita "las normas de negocio"


    }
}
/*
 BasiCalculator sut = new BasicCalculator();
 sut.suma(2,2) -> 4 -> Invariante

 if (sut.suma(2,2) ==4) {
 GREEN -> PASA
 } else {
    RED -> TEST NO PASA
    }

 Aserciones

 AssertEquals(sut.suma(2,2), 4)
             (Actual, Expected)


 CurrencyConverter sut = new CurrencyConverter();

 sut.convert("EUR", "USD", 3);

 convert a nivel interno

 CurrencyRateFetcher crf = new CurrencyRateFetcher();
 crf.fetch("EUR")
 crf.fetch("USD")

 BasiCalculator bc = new BasicCalculator();
 return bc.mult(3,1.2);


 Mocks -->  Construcción técnica que sirve para simular llamadas.
 - Stub: Pared que no tiene más funcionalidad que devolver un valor fijo, o confirmar que se ha llamado.
 - Mock:
 - Spy: Se llama a una función x veces, y te informa de como y que resultados te ha dado.

]/
 */