package com.techu.apitechudb.services;


import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    public List<PurchaseModel> findAll(){
        System.out.println("findAll en PurchaseService");

        return this.purchaseRepository.findAll();
    }
    /*
        public PurchaseModel findById(String id) {
            System.out.println("findById en PurchaseService");
    
            return this.PurchaseRepository.findById(id);
    
            for (PurchaseModel Purchase: this.PurchaseRepository.findAll()){
                Purchase.getId();
            }
        }
    */
    public Optional<PurchaseModel> findById(String id){
        System.out.println("Optional en PurchaseService");
/*        if (this.objectProperty != null)
            if (this.objectProperty.getProperty() != null)
                if (this.objectProperty.getProperty().getAnother() != null)
                    if (this.objectProperty.getProperty().getAnother().getYetAnotherProperty())*/

        return this.purchaseRepository.findById(id);
    }

    public PurchaseModel add(PurchaseModel Purchase){
        System.out.println("add en PurchaseService");

        return this.purchaseRepository.save(Purchase);
    }

}
