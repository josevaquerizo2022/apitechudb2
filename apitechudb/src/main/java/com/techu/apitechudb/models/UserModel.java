package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class UserModel {

    @Id
    private String id;
    private String name;
    private int age;

    //se hace un constructor sin parametros
    public UserModel() {
    }
    //se hace otro constructor con todos los parametros
    public UserModel(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    //además incluimo stodos los getter y setter
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}