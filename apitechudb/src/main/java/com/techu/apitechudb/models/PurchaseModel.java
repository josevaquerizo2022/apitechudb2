package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.aggregation.VariableOperators;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "purchase")
public class PurchaseModel {


    @Id
    private String id;
    private String userId;
    private Float amount;
    //private VariableOperators.Map purchaseItems;
    private String purchaseItems;


    public PurchaseModel() {
    }

//    public PurchaseModel(String id, String userId, Float amount, VariableOperators.Map purchaseItems) {
public PurchaseModel(String id, String userId, Float amount, String purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        //this.purchaseItems = VariableOperators.Map;
        this.purchaseItems = purchaseItems;


    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() { return this.userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public Float getAmount() { return this.amount; }

    public void setAmount(Float amount) {this.amount = amount; }

    public String getPurchaseItems() { return this.purchaseItems; }

    public void setPurchaseItems(String purchaseItems) {this.purchaseItems = purchaseItems;}

//    public VariableOperators.Map getPurchaseItems() { return this.purchaseItems; }

//    public void setPurchaseItems(VariableOperators.Map purchaseItems) {this.purchaseItems = purchaseItems;}
}

